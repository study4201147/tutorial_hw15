using Entities;
using Game.Tutorial.Gameplay;
using Game.Tutorial.UI;
using GameSystem;
using UnityEngine;

namespace Game.Tutorial
{
    [AddComponentMenu("Tutorial/Step «Kill Enemy»")]
    public sealed class KillEnemyStepController : TutorialStepController
    {
        [SerializeField]
        private MetaConfig config;

        [SerializeField]
        private KillEnemyManager enemyManager;

        [SerializeField]
        private PanelShower panelShower;

        private readonly KillEnemyInspector actionInspector = new();

        private ScreenTransform screenTransform;

        public override void ConstructGame(GameContext context)
        {
            this.screenTransform = context.GetService<ScreenTransform>();

            this.enemyManager.Construct(context);
            this.panelShower.Construct(this.config);
            base.ConstructGame(context);
        }

        protected override async void OnStart()
        {
            var enemy = await this.enemyManager.SpawnEnemy();
            this.actionInspector.Inspect(enemy, this.OnEnemyDestroyed);
            this.panelShower.Show(this.screenTransform.Value);
        }

        private void OnEnemyDestroyed(IEntity enemy)
        {
            this.panelShower.Hide();
            this.StartCoroutine(this.enemyManager.DestroyEnemy(enemy as MonoEntity));
            this.NotifyAboutCompleteAndMoveNext();
        }
    }
}