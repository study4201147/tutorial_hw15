using System;
using Game.GameEngine.GameResources;
using Game.Gameplay.Player;

namespace Game.Tutorial
{
    public sealed class TakeResourceFromConveyorInspector
    {
        private ConveyorVisitInteractor conveyorInteractor;

        private ResourceStorage resourceStorage;

        private ResourceTypeStepConfig config;
        
        private Action callback;

        public void Construct(ConveyorVisitInteractor conveyorInteractor, ResourceStorage resourceStorage, 
            ResourceTypeStepConfig config)
        {
            this.conveyorInteractor = conveyorInteractor;
            this.resourceStorage = resourceStorage;
            this.config = config;
        }

        public void Inspect(Action callback)
        {
            this.callback = callback;
            resourceStorage.OnResourceChanged += OnResourceChanged;
        }

        private void OnResourceChanged(ResourceType type, int amount)
        {
            if (conveyorInteractor.OutputZone.IsEntered && type == config.targetResourceType)
            {
                CompleteQuest();
            }
        }

        private void CompleteQuest()
        {
            resourceStorage.OnResourceChanged -= OnResourceChanged;
            callback?.Invoke();
        }
    }
}