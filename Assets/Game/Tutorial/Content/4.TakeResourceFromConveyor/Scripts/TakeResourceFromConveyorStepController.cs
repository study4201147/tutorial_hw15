using Game.Gameplay.Player;
using Game.Tutorial.App;
using Game.Tutorial.Gameplay;
using Game.Tutorial.UI;
using GameSystem;
using UnityEngine;

namespace Game.Tutorial
{
    [AddComponentMenu("Tutorial/Step «Take Resource From Conveyor»")]
    public sealed class TakeResourceFromConveyorStepController : TutorialStepController
    {
        [SerializeField]
        private ResourceTypeStepConfig config;

        [SerializeField]
        private PanelShower panelShower = new();

        [SerializeField]
        private Transform pointerTransform;

        private PointerManager pointerManager;

        private NavigationManager navigationManager;

        private ScreenTransform screenTransform;
        
        private readonly TakeResourceFromConveyorInspector inspector = new();

        public override void ConstructGame(GameContext context)
        {
            this.pointerManager = context.GetService<PointerManager>();
            this.navigationManager = context.GetService<NavigationManager>();
            this.screenTransform = context.GetService<ScreenTransform>();

            var conveyorInteractor = context.GetService<ConveyorVisitInteractor>();
            var resourceStorage = context.GetService<ResourceStorage>();
            this.inspector.Construct(conveyorInteractor, resourceStorage, config);
            this.panelShower.Construct(this.config);

            base.ConstructGame(context);
        }

        protected override void OnStart()
        {
            TutorialAnalytics.LogEventAndCache("tutorial_step_4__take_resource_from_conveyor_started");
            
            this.inspector.Inspect(callback: this.NotifyAboutCompleteAndMoveNext);

            Vector3 targetPosition = this.pointerTransform.position;
            this.pointerManager.ShowPointer(targetPosition, this.pointerTransform.rotation);
            this.navigationManager.StartLookAt(targetPosition);
            this.panelShower.Show(this.screenTransform.Value);
        }

        protected override void OnStop()
        {
            TutorialAnalytics.LogEventAndCache("tutorial_step_4__take_resource_from_conveyor_completed");
            
            this.panelShower.Hide();
            this.navigationManager.Stop();
            this.pointerManager.HidePointer();
        }
    }
}