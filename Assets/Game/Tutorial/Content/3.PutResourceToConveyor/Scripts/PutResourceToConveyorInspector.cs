using System;
using Entities;
using Game.Gameplay.Player;

namespace Game.Tutorial
{
    public sealed class PutResourceToConveyorInspector
    {
        private ConveyorVisitInteractor conveyorInteractor;
        
        private Action callback;

        public void Construct(ConveyorVisitInteractor conveyorInteractor)
        {
            this.conveyorInteractor = conveyorInteractor;
        }

        public void Inspect(Action callback)
        {
            this.callback = callback;
            conveyorInteractor.InputZone.OnEntered += OnResourcePut;
        }

        private void OnResourcePut(IEntity _)
        {
            conveyorInteractor.InputZone.OnEntered -= OnResourcePut;
            callback?.Invoke();
        }
    }
}