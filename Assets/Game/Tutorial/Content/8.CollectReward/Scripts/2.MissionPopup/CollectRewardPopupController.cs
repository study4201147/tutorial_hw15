using Game.Meta;
using Game.Tutorial.Gameplay;
using GameSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Tutorial
{
    [AddComponentMenu("Tutorial/Step «Collect Reward Popup»")]
    public sealed class CollectRewardPopupController : TutorialStepController
    {
        [SerializeField]
        private GameObject missionCursor;

        [SerializeField]
        private Transform fading;

        [Header("Close")]
        [SerializeField]
        private Button closeButton;

        [SerializeField]
        private GameObject closeCursor;

        private readonly CollectRewardInspector inspector = new();

        private void Awake()
        {
            this.missionCursor.SetActive(false);
            this.closeCursor.SetActive(false);
            this.closeButton.interactable = false;
        }

        public override void ConstructGame(GameContext context)
        {
            var missionManager = context.GetService<MissionsManager>();
            this.inspector.Construct(missionManager);

            base.ConstructGame(context);
        }

        public void Show()
        {
            this.inspector.Inspect(this.OnQuestFinished);
            this.missionCursor.SetActive(true);
        }

        private void OnQuestFinished()
        {
            this.missionCursor.SetActive(false);
            this.closeCursor.SetActive(true);
            this.fading.SetAsLastSibling();

            this.closeButton.interactable = true;
            this.closeButton.onClick.AddListener(this.OnCloseClicked);
            
            this.NotifyAboutComplete();
        }
        
        private void OnCloseClicked()
        {
            this.closeButton.onClick.RemoveListener(this.OnCloseClicked);
            this.closeCursor.SetActive(false);
            this.NotifyAboutMoveNext();
        }
    }
}