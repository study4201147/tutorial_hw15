using System;
using Game.Meta;

namespace Game.Tutorial
{
    public sealed class CollectRewardInspector
    {
        private MissionsManager missionsManager;
        
        private Action callback;
        
        public void Construct(MissionsManager missionsManager)
        {
            this.missionsManager = missionsManager;
        }
        
        public void Inspect(Action callback)
        {
            this.callback = callback;
            this.missionsManager.OnRewardReceived += OnRewardReceived;
        }

        private void OnRewardReceived(Mission _)
        {
            this.missionsManager.OnRewardReceived -= OnRewardReceived;
            this.callback?.Invoke();
        }
    }
}