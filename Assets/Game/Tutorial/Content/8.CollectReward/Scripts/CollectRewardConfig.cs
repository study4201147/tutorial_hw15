using Game.GameEngine;
using UnityEngine;

namespace Game.Tutorial
{
    [CreateAssetMenu(
        fileName = "Config «Collect Reward»",
        menuName = "Tutorial/Config «Collect Reward»"
    )]
    public sealed class CollectRewardConfig : MetaConfig
    {
        [Header("Quest")]
        [SerializeField]
        public WorldPlaceType worldPlaceType =  WorldPlaceType.TAVERN;
    }
}