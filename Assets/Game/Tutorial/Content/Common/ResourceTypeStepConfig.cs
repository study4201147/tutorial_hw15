using Game.GameEngine.GameResources;
using UnityEngine;

namespace Game.Tutorial
{
    [CreateAssetMenu(
        fileName = "Tutorial Step With Resource Type", 
        menuName = "Tutorial/New Tutorial Step With Resource Type")]
    public class ResourceTypeStepConfig : MetaConfig
    {
        [Header("Quest")]
        [SerializeField]
        public ResourceType targetResourceType;
    }
}