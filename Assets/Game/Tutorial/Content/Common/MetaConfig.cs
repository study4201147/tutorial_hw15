using Game.Localization;
using UnityEngine;

namespace Game.Tutorial
{
    [CreateAssetMenu(
        fileName = "Tutorial Step Config", 
        menuName = "Tutorial/New Tutorial Step Config")]
    public class MetaConfig : ScriptableObject
    {
        [Header("Meta")]
        [TranslationKey]
        [SerializeField]
        public string title;

        [SerializeField]
        public Sprite icon;
    }
}