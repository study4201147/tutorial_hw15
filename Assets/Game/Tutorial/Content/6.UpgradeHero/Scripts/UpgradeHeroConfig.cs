using Game.GameEngine;
using Game.Meta;
using UnityEngine;

namespace Game.Tutorial
{
    [CreateAssetMenu(
        fileName = "Config «Upgrade Hero»",
        menuName = "Tutorial/Config «Upgrade Hero»"
    )]
    public sealed class UpgradeHeroConfig : MetaConfig
    {
        [Header("Quest")]
        [SerializeField]
        public UpgradeConfig upgradeConfig;
        
        [SerializeField]
        public WorldPlaceType worldPlaceType =  WorldPlaceType.BLACKSMITH;

        [SerializeField]
        public PopupName requiredPopupName = PopupName.HERO_UPGRADES;
        
        [SerializeField]
        public int targetLevel = 3;
    }
}