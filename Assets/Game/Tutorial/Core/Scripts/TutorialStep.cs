namespace Game.Tutorial
{
    public enum TutorialStep
    {
        WELCOME = 0,
        HARVEST_RESOURCE = 1,
        PUT_RESOURCE_TO_CONVEYOR = 5,
        TAKE_RESOURCE_FROM_CONVEYOR = 6,
        SELL_RESOURCE = 2,
        UPGRADE_HERO = 3,
        DESTROY_ENEMY = 4,
        COLLECT_REWARD = 7,
    }
}