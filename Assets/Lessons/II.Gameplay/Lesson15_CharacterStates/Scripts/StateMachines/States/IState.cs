namespace Lessons.StateMachines.States
{
    public interface IState
    {
        public void Enter();

        public void Exit();
    }
}