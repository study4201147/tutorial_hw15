namespace Lessons.Architecture.Components
{
    public interface IAttackComponent
    {
        void Attack(Entity target);
    }
}